interface Project {
  title: string
  description: string
  href?: string
  imgSrc?: string
}

const projectsData: Project[] = [
  {
    title: 'MLOPs human pose estimation end-to-end',
    description: `This project focused on developing an end-to-end MLOps system that integrates OpenPose human pose estimation, facilitating seamless development and production workflows.`,
    imgSrc: '/static/images/projects/mlops-human-pose-estimation/mlops-openpose-architecture.png',
    href: 'https://github.com/ToDucThanh/MLOPS-human-pose-estimation',
  },
]

export default projectsData
