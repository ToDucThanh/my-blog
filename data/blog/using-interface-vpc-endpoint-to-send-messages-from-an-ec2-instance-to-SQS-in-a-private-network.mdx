---
title: 'Using Interface VPC Endpoint to send messages from an EC2 instance to SQS in a private network'
date: '2024-08-25'
lastmod: '2024-08-28'
tags: ['Cloud Computing', 'AWS', 'EC2', 'VPC', 'VPC Endpoint', 'SQS', 'IAM']
draft: false
summary: 'How to securely send messages from EC2 instances to SQS queues? Use Interface VPC Endpoint!!!'
---

![architecture](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/architecture.png)

# Table of content

<TOCInline toc={props.toc} exclude="Table of content"/>

# 1. Create network configurations

## 1.1 Create a VPC

Navigate to **VPC management console** and select **Create VPC**. Then, change the settings as follow:

![create-a-vpc](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/create-a-vpc.png)

> [!NOTE]
> Here are the setting details:
> 1. **Resources to create**: `VPC only`.
> 2. **IPv4 CIDR**: `10.10.0.0/16`.
> 3. **Name**: `MainVPC`.

Finally, click on **Create VPC**.

Review the details of the newly created VPC. Ensure that Enable DNS resolution and DNS Hostname is disabled. Then, select
**Action** >> **Edit VPC settings**:

![vpc-detailed](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/vpc-detailed.png)

Select **Enable DNS hostnames**, and **Save**:

![enable-dns-hostname](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/enable-dns-hostname.png)

## 1.2 Create Subnets

### 1.2.1 Create a Public Subnet

In the **VPC navigation interface**, select **Subnets**. Then select **Create subnet**:

![create-subnet-interface](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/create-subnet-interface.png)

In the **Create Subnet** pane, at **VPC ID**, choose `MainVPC`:

![select-MainVPC](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/select-MainVPC.png)

Fill in information for the **Public Subnet**, then click on **Create subnet**:

![publib-subnet-settings](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/public-subnet-settings.png)

> [!NOTE]
> Here are the setting details of the **Public Subnet**:
> 1. **Subnet name**: `public-subnet`.
> 2. **Availability Zone**: `us-east-1a`.
> 3. **IPv4 subnet CIDR block**: `10.10.10.0/24`.

Review the details of the newly created public subnet. Now, we will need to enable its **Auto-assign public IPv4 address** function.
Click on **Action** > **Edit subnet settings**:

![public-subnet-detailed](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/public-subnet-detailed.png)

Choose **Enable auto-assign public IPv4 address** and **Save**:

![enable-auto-assign-ipv4](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/enable-auto-assign-ipv4.png)

### 1.2.2 Create a Private Subnet

Go back to **VPC navigation interface**, select **Subnets**. Then select **Create subnet**:

![create-private-subnet](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/create-private-subnet.png)

In the **Create Subnet** pane, at **VPC ID**, choose `MainVPC`:

![select-MainVPC](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/select-MainVPC.png)

Fill in information for the **Private Subnet**, then click on **Create subnet**:

![private-subnet-settings](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/private-subnet-settings.png)

> [!NOTE]
> Here are the setting details of the **Private Subnet**:
> 1. **Subnet name**: `private-subnet`.
> 2. **Availability Zone**: `us-east-1b`.
> 3. **IPv4 subnet CIDR block**: `10.10.20.0/24`.

> [!IMPORTANT]
> **We have created 2 Subnets successfully.**
>
> ![two-subnets-detailed](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/two-subnets-detailed.png)

## 1.3 Create an Internet Gateway

In the **VPC navigation interface**, select **Internet gateways**. Then select **Create internet gateway**:

![create-internet-gateway](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/create-internet-gateway.png)

Enter **Name tag**: `internet-gateway`, then click on **Create internet gateway**:

![set-name-igw](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/set-name-igw.png)

Click on **Attach to a VPC**:

![attach-to-a-vpc](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/attach-to-a-vpc.png)

Choose `MainVPC` VPC to attach and click on **Attach internet gateway**:

![choose-vpc-to-attach-igw](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/choose-vpc-to-attach-igw.png)

Once attached successfully, the **State** of the internet gateway will change to **Attached**:

![attached](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/attached.png)

## 1.4 Create Route Tables

### 1.4.1 Create a Public Route Table

In the **VPC navigation interface**, select **Route tables**. We will edit the default route table of `MainVPC`.
Select route table of `MainVPC` and set it a name: `public-route-table`. Then, **Save**:

![set-a-name-route-table](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/set-a-name-to-route-table.png)

Select **Routes** tab and click on **Edit routes**:

![edit-routes](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/edit-routes.png)

In the **Edit routes** pane:
- Click on **Add route**.
- Fill in the **Destination CIDR**: `0.0.0.0/0` representing the Internet.
- In the **Target section**, select **Internet Gateway**, then choose the created Internet Gateway (Gateway ID auto-fills).
- Click on **Save changes**.

![edit-route-settings](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/edit-route-settings.png)

> [!IMPORTANT]
> **We have created a public route table, to which an internet gateway is attached.**
>
> ![route-table-igw](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/public-route-table-and-igw.png)

Next, we will associate the `public-subnet` with the `public-route-table`. Select **Subnet associations** tab and click on
**Edit subnet associations**:

![edit-subnet-association-public](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/edit-subnet-association-public.png)

Choose `public-subnet` and click on **Save associations**

![save-public-association](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/save-public-subnet-association.png)

### 1.4.1 Create a Private Route Table

Go back to **VPC navigation interface**, select **Route tables**. Then select **Create route table**.

At **Route table settings** pane, enter a name: `private-route-table` and select `MainVPC`. Then, click on **Create route table**:

![private-route-table-settings](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/private-route-table-settings.png)

Similarly, we will associate the `private-subnet` with the `private-route-table` as we have done with public ones.
With **Private Route Table**, we do not need **Internet Gateway**.

> [!IMPORTANT]
> **Verify that we have created two route tables with the corresponding subnets.**
>
> ![final-route-table](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/final-route-tables.png)

## 1.5 Create a VPC Endpoint

### 1.5.1 Create a Security Group for the VPC Endpoint

This **Endpoint** will reside in the **Private Subnet** and we will create a **Securiy Group** that allows all traffic from the 
**Private Subnet**

In **VPC navigation interface**, select **Security groups**. Then select **Create security group**:

![create-sg](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/create-a-security-group.png)

In the **Create security group** pane, fill in the information as follow:

![sg-private-subnet](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/create-security-group-private-subnet.png)

Click on **Create security group**.

> [!NOTE]
> **Endpoint Security Group** information:
> 1. **Security group name**: `endpoint-security-group`.
> 2. **Description**: Allow all traffic from private subnet.
> 3. **VPC**: `MainVPC`.
> 4. **Inbound rules**:  
>       **Type**: All traffic, **Source**: Custom, `10.10.20.0/24`.

### 1.5.2 Create the VPC Endpoint for SQS

In **VPC navigation interface**, select **Endpoints**. Then select **Create endpoint**:

![create-endpoint](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/vpc-endpoint.png)

In **Create endpoint** pane:
- Enter the name: `MyEndpoint`.
- At **Service category**, choose **AWS services**.
- At **Services**, search for `sqs` and select **com.amazonaws.us-east-1.sqs**.

![endpoint-setting-1](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/endpoint-setting-1.png)

- At **VPC**, choose `MainVPC`.
- At **Subnets**, select **us-east-1b** and choose **private subnet**.

![endpoint-setting-2](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/endpoint-setting-2.png)

- At **Security groups**, choose the newly created `endpoint-security-group`.

![endpoint-setting-3](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/endpoint-setting-3.png)

- Click on **Create endpoint** and wait till the **Status** be **Available**.

![endpoint-setting-4](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/endpoint-setting-4.png)

# 2. Create EC2 instances

## 2.1 Create an EC2 instance in the public subnet (Public EC2)

### 2.1.1 Set up

Access **the AWS Management Console**, navigate to **EC2** and choose **Launch instances**

In the **Launch an instance** interface:
- Enter the name `public-ec2` at **Name and tags**.
- Choose **Amazon Linux 2023 AMI**.

![launch-an-instance-1](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/launch-an-instance-1.png)

- Select **Instance type** be **t2.micro** and click on **Create new key pair**:

![launch-an-instance-2](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/launch-an-instance-type-2.png)

- On the **Create key pair** pop-up window, fill in information:

![create-key-pair](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/create-key-pair.png)

> [!NOTE]
> A key file named `public-ec2-key.pem` will be downloaded to your host machine; store it in a safe place.

- **Edit** the network settings:
    - Select the **VPC**: `MainVPC`.
    - Choose the **Subnet**: `public-subnet`.
    - **Auto-assign public IP**: **Enable**.
    - **Create security group** and set **Security group name**: `public-security-group`.
    - **Description**: Allow traffic from the Internet.

![network-settings](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/network-settings.png)

- Click on **Launch instance** and wait untill completion: the **Status** check shows **2/2 checks passed**:

![public-ec2](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/public-ec2.png)

> [!NOTE]
> **Public EC2** instance information:
> 1. **Name**: `public-ec2`.
> 2. **AMI**: **Amazon Linux 2023 AMI**.
> 3. **Instance type**: **t2.micro**.
> 4. **Key pair**: `public-ec2-key.pem`.
> 5. **VPC**: `MainVPC`.
> 6. **Subnet**: `public-subnet`.
> 7. **Auto-assign public IP**: **Enable**.
> 8. **Security group name**: `public-security-group`.
> 9. **Description**: Allow traffic from the Internet.
> 10. **Inbound rules**:  
>       **Type**: SSH, **Port range**: 22, **Protocol**: TCP, **Source**: `0.0.0.0/0`

### 2.1.2 Test connection

Since the OS of my host machine is **Ubuntu**, I will connect to the **Public EC2** instance using the terminal.

Firstly, we need to change the key's permission to **400**:

```bash
sudo chmod 400 public-ec2-key.pem
```

![change-key-permission](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/change-key-permission.png)

Then, connect to the instance using the below command:

```bash
ssh -i public-ec2-key.pem ec2-user@[your-ec2-public-ip-address]
```

> [!TIP]
> `[your-ec2-public-ip-address]` can be found on the **EC2** instance page:
>
> ![find-public-ipv4](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/find-public-ipv4.png)

Test the connect to the Internet:

```bash
ping google.com
```

![test-connection](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/test-connection.png)

## 2.2 Create an EC2 instance in the private subnet (Private EC2)

### 2.2.1 Set up

Access **the AWS Management Console**, navigate to **EC2** and choose **Launch instances**

In the **Launch an instance** interface:
- Enter the name `private-ec2` at **Name and tags**.
- Choose **Amazon Linux 2023 AMI**.
- Select **Instance type** be **t2.micro** and click on **Create new key pair**.
- On the **Create key pair** pop-up window, fill in information:

![private-key-pair](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/private-key-pair.png)

> [!NOTE]
> A key file named `private-ec2-key.pem` will be downloaded to your host machine; store it in a safe place.

- **Edit** the network settings:
    - Select the **VPC**: `MainVPC`.
    - Choose the **Subnet**: `private-subnet`.
    - **Auto-assign public IP**: **Disable**.
    - **Create security group** and set **Security group name**: `private-security-group`.
    - **Description**: Allow SSH from public subnet only.
    - **Inbound Security Group Rules**:
        - **Source type**: **Custom**.
        - **Source**: `10.10.10.0/24`.

![private-network-setting](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/private-network-configurations.png)

- Click on **Launch instance** and wait untill completion: the **Status** check shows **2/2 checks passed**

> [!NOTE]
> **Private EC2** instance information:
> 1. **Name**: `private-ec2`.
> 2. **AMI**: **Amazon Linux 2023 AMI**.
> 3. **Instance type**: **t2.micro**.
> 4. **Key pair**: `private-ec2-key.pem`.
> 5. **VPC**: `MainVPC`.
> 6. **Subnet**: `private-subnet`.
> 7. **Auto-assign public IP**: **Disable**.
> 8. **Security group name**: `private-security-group`.
> 9. **Description**: Allow SSH from public subnet only.
> 10. **Inbound rules**:  
>       **Type**: SSH, **Port range**: 22, **Protocol**: TCP, **Source**: `10.10.10.0/24`

### 2.2.2 Attach a IAM role to grant the Private Subnet full access to SQS.

Navigate to **IAM interface**, select **Roles**. Then select **Create role**:

![create-role](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/create-role.png)

On **Select trusted entity** page, select **Trusted entity type** as **AWS service**. Select a service as EC2 
from the list to view its use case, and then choose a **Use case** as **EC2** and click on **Next**:

![selecte-trusted-entity](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/selectTrustedEntity.png)

On **Add permissions** page, select the policy from the list of AWS managed policies that grants 
your instances access to the resources. In this case, choose **AmazonSQSFullAccess** policy. Click on **Next**:

![sqs-full-access-policy](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/sqs-access-policy.png)

Enter **Role name**: `ec2FullAccessSQS`, then click on **Create role**.

![sqs-policy-name](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/sqs-policy-name.png)

Now, navigate to **EC2 console**:
- Select **Instance**.
- Select **private-ec2**.
- Click on **Action**.
- Choose **Security**.
- Choose **Modify IAM role**.

![modify-role-private-ec2](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/modify-role-private-ec2.png)

On **Modify IAM role** page, select **IAM role** be `ec2FullAccessSQS`. Then, click on **Update IAM role**.

![update-iam-role-private-ec2](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/update-iam-role-privarte-ec2.png)

# 3. Create a SQS queue

Now, we will create a simple queue.

Navigate to **SQS** interface, select **Create queue**:

![create-queue](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/create-queue.png)

Choose **Type**: **Standard** ans set the **Name**: `simpleQueue`. Leave everything else at the default settings, then, click on **Create queue**.

![queue-setting](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/sqs-settings.png)

# 4. Send messages from the EC2 instance to SQS.

## 4.1 Connect to the Private EC2 from the Public EC2

First, copy the **Private EC2's key** to the **Public EC2**:

On your host machine, locate the key and execute the following command in the terminal:

```bash
scp -i public-ec2-key.pem private-ec2-key.pem ec2-user@[your-ec2-public-ip-address]:/home/ec2-user
```

In your **Public EC2** terminal: 
- Check if the key is there:

![check-key](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/check-key.png)

- Change the permission of the private key and connect to the **Private EC2**:

```bash
sudo chmod 400 private-ec2-key.pem
ssh -i private-ec2-key.pem ec2-user@10.10.20.139
```

> [!TIP]
> `10.10.20.139` is my private ipv4 of my **Private EC2**, you replace with yours.

![connect-to-private-ec2](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/connect-to-private-ec2.png)

## 4.2 Send a message to SQS

In your **Public EC2** terminal, execute the following command:

```bash
aws sqs send-messages --queue-url [your-queue-url] --message-body "Message from the private EC2" --endpoint-url [your-endpoint-url] --region us-east-1
```

![send-message-command](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/send-message-command.png)

- `[your-queue-url]` can be found at the **SQS** interface:

![queue-url](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/queue-url.png)

- `[your-endpoint-url]` can be found at the **VPC** interface:

![endpoint-url](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/endpoint-url.png)

Now, the message was sent, navigate to **SQS** interface to receive the message:

![send-and-receive-message](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/send-and-receive-message.png)

Click on **Poll for messsage**:

![poll-messages](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/poll-messages.png)

Clik on the message, and the body content is displayed:

- ![click-on-message](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/click-on-message.png)
- ![message-from-ec2](/static/images/blogs/using-interface-vpc-endpoint-to-send-messages-from-an-ec2-instance-to-sqs-in-a-private-network/message-from-ec2.png)

**We have successully send the message to the SQS queue.**

# 5. Summary

This blog post explores the process of setting up and utilizing an **Interface VPC Endpoint** to enable secure communication between an **EC2**
instance and **Amazon SQS** within a **private network** environment. By implementing this solution, organizations can enhance the security and
efficiency of their message queuing systems within AWS, ensuring that sensitive data remains within their private network while leveraging
the scalability and reliability of **Amazon SQS**.
