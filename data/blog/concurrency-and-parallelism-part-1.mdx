---
title: 'Concurrency and parallelism (part 1)'
date: '2024-06-05'
lastmod: '2024-06-07'
tags: ['python', 'concurrency', 'parallelism', 'thread', 'process']
draft: true
summary: 'Concurrency and parallelism are key concepts in programming that enable the execution of multiple tasks simultaneously, improving the efficiency and performance of applications. Although often used interchangeably, they refer to different techniques.'
---

## 1. Definition

Let's understand some basic keywords 😗

### 1.1 Concurrency

Concurrency allows a computer to handle multiple tasks as if they are happening at the same time. On a single-core CPU, the operating system quickly switches between different programs, interweaving their execution to create the appearance of simultaneous operation

### 1.2 Parallelism

On the other hand, parallelism involves performing multiple tasks at the same time. With multiple CPU cores, a computer can truly execute several programs at once, with each core processing instructions from a different program, enabling simultaneous progress.

> [!NOTE]
> (Reveal some secrets!!! 🕵️‍♂️) Python provides various approaches for writing concurrent programs. Threads offer a limited degree of concurrency, whereas coroutines allow for a large number of concurrent functions. Additionally, Python can perform parallel tasks using system calls, subprocesses, and C extensions. However, **achieving true parallel execution with concurrent Python code can be challenging**. It's crucial to understand how to effectively leverage Python in these diverse scenarios.

### 1.3 Mutual-exclusion lock

A mutual-exclusion lock, commonly referred to as a **Mutex**, is a synchronization mechanism used to prevent concurrent processes or threads from accessing a shared resource or critical section simultaneously. The purpose of a mutex is to ensure that only one thread can access the shared resource at any given time, thereby avoiding race conditions and ensuring data consistency.

**Key Concepts of Mutex**:

`Lock and Unlock`:

- Lock: When a thread wants to enter a critical section, it must first acquire the lock. If another thread has already locked the mutex, the requesting thread will be blocked until the mutex is unlocked.
- Unlock: Once the thread finishes its operation in the critical section, it releases the lock, allowing other threads to acquire it.

`Critical Section`:

- This is the part of the code that accesses shared resources and needs to be protected by the mutex to prevent concurrent access.

`Race Conditions`:

- A situation where the behavior of software depends on the relative timing of events, such as the order of execution of threads. Mutexes help prevent race conditions by ensuring that only one thread can access the critical section at a time.

## 2. Understand threads in Python

In languages like C++ or Java, having multiple threads of execution enables a program to utilize multiple CPU cores simultaneously. Although Python supports multiple threads of execution, there is a mechanism that hinders true parallelism: 
**the Global Interpreter Lock (GIL)**. So what exactly is this GIL 😮‍💨 ?

There are many implemtentations of Python: CPython, Jython, PyPy, Stackless Python and others. CPython is the most standard and popular implementation. The interpreter is written in C.
When CPython runs a Python program, it does so in two main steps.

- **Step 1: Parsing and Compiling:**
    - Parsing: CPython first reads the source text of the Python program.
    - Compiling: It then compiles this source text into bytecode. Bytecode is a lower-level, intermediate representation of the program. It consists of instructions that are 8 bits (1 byte) in size.
    Since Python 3.6, this has been updated to "wordcode" with 16-bit (2-byte) instructions.

- **Step 2: Bytecode Execution:**
    - Stack-based Interpreter: After compiling to bytecode, CPython runs this bytecode using a stack-based interpreter. This means it uses a stack data structure to manage function calls and other control structures.

While executing bytecode, the interpreter maintains certain state information that must remain consistent and coherent. To ensure this coherence, 
the Global Interpreter Lock (GIL) is used. The GIL is a [Mutex](#13-mutual-exclusion-lock) that protects access to Python objects, preventing multiple native 
threads from executing Python bytecode simultaneously. This ensures that only one thread runs in the interpreter at a time, preventing the program 
from being affected by [preemptive multithreading](https://en.wikipedia.org/wiki/Preemption_(computing)), where one thread interrupts and takes control from another thread.
An unexpected interruption may corrupt the interpreter state, if it comes at an unexpected time, for instance [garbage collection reference counts](https://en.wikipedia.org/wiki/Garbage_collection_(computer_science)#:~:text=Reference%20counting%20garbage%20collection%20is,when%20a%20reference%20is%20destroyed). 
The GIL prevents such interruptions and assures proper bytecode execution with the CPython implementation and its C-extension modules.
