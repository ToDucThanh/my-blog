---
title: 'Securing EC2 instances: Why AWS IAM roles are a game-changer'
date: '2024-08-12'
lastmod: '2024-08-12'
tags: ['Cloud Computing', 'AWS', 'IAM', 'EC2']
draft: false
summary: 'Imagine a scenario where a malicious hacker gains access to your EC2 instances configured with hardcoded credentials. This situation could lead to catastrophic consequences!!! 😰'
---

![background-image](/static/images/blogs/securing-ec2-intances-why-aws-iam-roles-are-a-game-changer/ec2FullAcessS3.png)

# 1. Naive approach

A common practice among novice AWS users who need to transfer data between **EC2** and **S3** is to configure credentials directly on the **EC2** instance. 
They typically SSH into the **EC2** instance and execute:

```bash
aws configure
```

Subsequently, they input the _Access Key_ and _Secret Access Key_, storing them locally on the **EC2** instance. While this method provides a quick solution to grant 
**EC2** full access to **S3**, it introduces significant security risks and violates AWS best practices. In a worst-case scenario, attackers could compromise 
the **EC2** instance and obtain these sensitive keys.

To mitigate this risk, we will implement a more secure approach using **IAM roles**.

# 2. Review of Key IAM Concepts

This section provides a brief overview of **AWS Users**, **User Groups**, **Roles**, and **Policies**. If you are familiar with these concepts, feel free to proceed 
to the next section.

These are fundamental components of **AWS Identity and Access Management (IAM)**, which is used to securely control access to AWS services and resources.


> [!NOTE]  
> _AWS Users_
> - Individual identities created in AWS IAM.
> - Represent people or applications that need access to AWS resources.
> - Have permanent long-term credentials (username and password for console access, access keys for programmatic access).

> [!NOTE]
> _AWS User Groups_
> - Collections of IAM users.
> - Allow you to manage permissions for multiple users at once.
> - Simplify access management by assigning permissions to groups rather than individual users.

> [!NOTE]
> _AWS Roles_
> - Similar to users, but intended to be assumed by entities that need temporary access.
> - Can be assumed by AWS services, applications, or users from other AWS accounts.
> - Don't have permanent credentials associated with them.
> - Provide temporary security credentials for role sessions.


> [!NOTE]
> _AWS Policies_
> - JSON documents that define permissions.
> - Specify what actions are allowed or denied on which AWS resources.
> - Can be attached to users, groups, or roles.
> - Two main types:
>   - a. Identity-based policies: Attached to users, groups, or roles.
>   - b. Resource-based policies: Attached to resources (e.g., S3 bucket policies).

These components work together to implement the principle of least privilege, ensuring that entities have only the permissions they 
need to perform their tasks.

# 3. Approach for Existing EC2 Instances

In this scenario, you have an existing **EC2** instance without an attached **IAM role**. You can verify this by checking the **IAM role** value 
on the **Details** tab of the instance, which should be empty.

![ec2-iam-role-empty-image](/static/images/blogs/securing-ec2-intances-why-aws-iam-roles-are-a-game-changer/ec2IAMRoleEmpty.png)

To create and attach a new **IAM role** to this **EC2** instance:

Step 1: Navigate to the IAM console at https://console.aws.amazon.com/iam/. In the navigation pane, choose **Roles** and click **Create role**.

![create-role-dashboard](/static/images/blogs/securing-ec2-intances-why-aws-iam-roles-are-a-game-changer/createRoleDashboard.png)

Step 2: On the **Select trusted entity** page, select **Trusted entity type** as **AWS service**. Select a service as EC2 from the list to view its use case, 
and then choose a **Use case** as **EC2** and click **Next**.

![selecte-trusted-entity](/static/images/blogs/securing-ec2-intances-why-aws-iam-roles-are-a-game-changer/selectTrustedEntity.png)

Step 3: On the **Add permissions** page, select the policy from the list of AWS managed policies that grants your instances access to the 
resources. In this case, choose `AmazonS3FullAccess` policy. Click **Next**.

![s3-full-access-policy](/static/images/blogs/securing-ec2-intances-why-aws-iam-roles-are-a-game-changer/s3FullAcessPolicy.png)

Step 4: On the **Name, review and create** page, enter a name for the role (e.g., `ec2FullAccessS3`) and click **Create role**.

![enter-role-name](/static/images/blogs/securing-ec2-intances-why-aws-iam-roles-are-a-game-changer/enterRoleName.png)

Step 5: Navigate to the **EC2 Dashboard** and select **Instance**. Choose the target instance, and from the **Actions** menu, 
select **Security** -> **Modify IAM role**.

![modify-iam-role](/static/images/blogs/securing-ec2-intances-why-aws-iam-roles-are-a-game-changer/choose-modify-iam-role.png)

Step 6: Select the newly created role (`ec2FullAcessS3`) from the dropdown menu and click **Update IAM Role**.

![update-iam-role](/static/images/blogs/securing-ec2-intances-why-aws-iam-roles-are-a-game-changer/updateIAMRole.png)

Step 7: Verify that the correct role is attached to the instance by checking the **EC2 dashboard**.

![ec2-role-attached](/static/images/blogs/securing-ec2-intances-why-aws-iam-roles-are-a-game-changer/ec2RoleAttached.png)

# 4. Approach for New EC2 Instances

When launching a new **EC2** instance, you can attach an **IAM role** during the creation process. First, create a new **IAM role** following steps 1-4 
from the [previous section](#3-approach-for-existing-ec2-instances).

During the **EC2** instance launch process, configure the **Advanced details** tab. In the **IAM instance profile** dropdown menu, select the appropriate 
role (e.g., `ec2FullAccessS3`).

![instance-profile-ec2](/static/images/blogs/securing-ec2-intances-why-aws-iam-roles-are-a-game-changer/instance-profile-ec2.png)

At this point, you may have questions regarding IAM instance profiles:
> What is an IAM instance profile?  
> And, what is the difference between an AWS IAM role and an instance profile?

For a comprehensive explanation, refer to [this article](https://medium.com/devops-dudes/the-difference-between-an-aws-role-and-an-instance-profile-ae81abd700d). 
Here's a brief summary:

> [!TIP]
> A **role** is _what can I do?_  
> An **instance profile** defines _who am I?_  
> An **IAM user** represents a person, an **instance profile** represents **EC2** instances.  
> An **instance profile** can contain only one **IAM role**. This limit cannot be increased.  
> When you create an **IAM Role** for **EC2** using the **AWS Management Console**, it creates both an **EC2 instance profile** as well as an **IAM role**.  
> However, if you are using the **AWS CLI**, **SDKs**, or **CloudFormation**, you will need to **EXPLICITLY** define both:  
> - An **IAM role** with policies and permissions, and 
> - An **EC2 instance profile** specifying which roles it can assume

# 5. Conclusion

Implementing AWS **IAM roles** for **EC2** instances represents a significant advancement in cloud security practices. By leveraging **IAM roles**, we can effectively eliminate 
the need for hardcoded credentials, thereby substantially reducing the risk of unauthorized access and potential security breaches.

Key takeaways from this approach include:
> [!IMPORTANT]
> 1. **Enhanced Security**: IAM roles provide a more secure method of granting permissions to EC2 instances, eliminating the risks associated with storing long-term access keys on instances.  
> 2. **Simplified Management**: With IAM roles, there's no need to manage and rotate access keys manually. AWS handles the issuance and rotation of temporary credentials automatically.  
> 3. **Principle of Least Privilege**: IAM roles allow for fine-grained control over permissions, ensuring that EC2 instances have only the access they need to perform their specific tasks.  
> 4. **Scalability**: As you launch new EC2 instances, you can easily assign pre-configured IAM roles, maintaining consistent and secure access policies across your infrastructure.  
> 5. **Compliance**: Using IAM roles aligns with AWS best practices and helps in meeting various compliance requirements by improving overall security posture.
